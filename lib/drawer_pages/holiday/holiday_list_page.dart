import 'package:flutter/material.dart';

class HolidayListPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    Container _getFestival(String festivalName, String date){
      return Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(festivalName,
                style: TextStyle(
                    color: Colors.black,
                  fontWeight: FontWeight.bold
                ),
              ),
              Text(date,
                style: TextStyle(
                    color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Holiday List',
            style: TextStyle(color: Colors.black)),
        iconTheme: IconThemeData(
            color: Colors.blue
        ),
        elevation: 1.0,
        backgroundColor: Colors.white,
        centerTitle: true,
        actions: <Widget>[
          FlatButton(
            color: Colors.white,
              onPressed: (){

            },
            child: Text("Download PDF",
            style: TextStyle(
              backgroundColor: Colors.transparent,
              color: Colors.grey,
              fontSize: 12.0
          ),))
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: _width/11, right: _width/11),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text("National Holidays",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),),
                  ],
                ),
              ),
              Container(
                height: 1.0,
                color: Colors.grey,
              //  margin: EdgeInsets.only(top: _height/50, bottom: _height/50),
              ),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
              _getFestival("Diwali", "20/10/2019"),
            ],
          ),
        ),
      ),
    );
  }
}