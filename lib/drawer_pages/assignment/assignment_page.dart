import 'package:flutter/material.dart';
import 'package:iviewsmart_student/drawer_pages/assignment/assignment_submission_page.dart';

class AssignmentPage extends StatefulWidget{

  @override
  _AssignmentPageState createState() => _AssignmentPageState();


}

class _AssignmentPageState extends State<AssignmentPage> {

  TextEditingController _subjectTextController;
  TextEditingController _teacherTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _subjectTextController = TextEditingController();
    _teacherTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _subjectTextController.dispose();
    _teacherTextController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    Container _proceedButton(){
      return Container(
        width: MediaQuery.of(context).size.width/1.3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [BoxShadow(
              color: Colors.black45,
              blurRadius: 5.0,
            ),]
        ),
        child: Text('Proceed',
          style: TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Assignment',
            style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 1.0,
        iconTheme: IconThemeData(
          color: Colors.blue
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: _width/11, right: _width/11),
          child: Column(
            children: <Widget>[
              Container(
                child: TextField(
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                  controller: _subjectTextController,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      labelText: 'Select your subject',
                      border: InputBorder.none
                  ),
                ),
              ),
              Container(
                height: 1.0,
                color: Colors.grey,
              ),
              Container(
                child: TextField(
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                  ),
                  controller: _teacherTextController,
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(15.0),
                      labelText: 'Select your teacher',
                      border: InputBorder.none
                  ),
                ),
              ),
              SizedBox(
                height: 7.0,
              ),
      InkWell(
        borderRadius: BorderRadius.circular(50.0),
        onTap: (){
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (BuildContext context) => AssignmentSubmissionPage())
          );
        },
        child: _proceedButton(),
      ),
            ],
          ),
        ),
      )
    );
  }
}