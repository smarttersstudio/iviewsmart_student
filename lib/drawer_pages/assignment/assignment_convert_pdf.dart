import 'package:flutter/material.dart';
class ConvertToPdf extends StatefulWidget{
  @override
  _ConvertToPdfState createState() => _ConvertToPdfState();
}

class _ConvertToPdfState extends State<ConvertToPdf> {
  @override
  Widget build(BuildContext context) {

    Container _proceedButton(){
      return Container(
        width: MediaQuery.of(context).size.width/1.3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [BoxShadow(
              color: Colors.black45,
              blurRadius: 5.0,
            ),]
        ),
        child: Text('Proceed',
          style: TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Assignment',
            style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 1.0,
        iconTheme: IconThemeData(
            color: Colors.blue
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            color: Colors.transparent,
            child: Container(
              height: MediaQuery.of(context).size.height/2,
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text("Upload PDF files only",style: TextStyle(
                     fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                  ),),
                  InkWell(
                    borderRadius: BorderRadius.circular(50.0),
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (BuildContext context) => ConvertToPdf())
                      );
                    },
                    child: _proceedButton(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}