import 'package:flutter/material.dart';
import 'package:iviewsmart_student/drawer_pages/assignment/assignment_convert_pdf.dart';
class AssignmentSubmissionPage extends StatefulWidget{
  @override
  _AssignmentSubmissionPageState createState() => _AssignmentSubmissionPageState();
}

class _AssignmentSubmissionPageState extends State<AssignmentSubmissionPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Container _downloadButton(){
      return Container(
        width: MediaQuery.of(context).size.width/7,
        height: 30.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(50.0),
        ),
        child: Text('Download',
          style: TextStyle(
              fontSize: 10.0,
              color: Colors.black,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }

    Container _submitButton(){
      return Container(
        width: MediaQuery.of(context).size.width/7,
        height: 30.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.blue,
          borderRadius: BorderRadius.circular(50.0),
        ),
        child: Text('Submit',
          style: TextStyle(
              fontSize: 10.0,
              color: Colors.black,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }

    Container _assignMentInfo(String _subject, String _time){
      return Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      text: _subject,
                      style: TextStyle(color: Colors.black,fontSize: 14.0, fontWeight: FontWeight.bold ),
                      children: [
                        TextSpan(text: '\n'),
                        TextSpan(
                          style: TextStyle(color: Colors.grey,fontSize: 12.0 ),
                          text: _time,
                        ),
                      ],
                    ),
                  ),
                  _downloadButton(),
                  InkWell(
                    borderRadius: BorderRadius.circular(20.0),
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (BuildContext context) => ConvertToPdf())
                      );
                    },
                    child: _submitButton(),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Container(
                  height: 1.0,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Assignment',
            style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 1.0,
        iconTheme: IconThemeData(
            color: Colors.blue
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              _assignMentInfo("Chemistry", "Last date of submission is 23/10/2019"),
              _assignMentInfo("Chemistry", "Last date of submission is 23/10/2019"),
              _assignMentInfo("Chemistry", "Last date of submission is 23/10/2019"),
            ],
          ),
        ),
      ),
    );
  }
}