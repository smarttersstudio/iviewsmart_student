import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/GalleryPage.dart';

class AlbumPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    _buildCard(ImageProvider image, String title, int imageCount, Object nextRoute){
      return Container(
        color: Colors.white,
        margin: EdgeInsets.only(left: 5.0, right: 5.0,top: 5.0),
        child: Container(
          padding: EdgeInsets.all(10.0),
          alignment: Alignment.centerLeft,
          child: ListTile(
            contentPadding: EdgeInsets.all(10.0),
            title: Text(title, textAlign: TextAlign.left,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold
              ),
            ),
            trailing: Wrap(
                children: <Widget>[
                  Text(imageCount.toString(), textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Colors.grey,
                    ),),
                  Icon(Icons.navigate_next),
                ]),
            leading: Container(
                height: _width/5,
                width: _width/5,
                padding: EdgeInsets.all(3.0),
                color: Colors.white,
                child: Image(
                  image: image,
                  fit: BoxFit.cover,
                ),
            ),
            onTap:(){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => nextRoute)
              );
            },
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        title: Text('Album',style: TextStyle(color: Colors.black)),
        centerTitle: true,
        elevation: 0.5,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _buildCard(AssetImage('dashboard_background.png'), 'School Party',25, GalleryPage('School Party')),
            _buildCard(AssetImage('dashboard_background.png'), 'School Trip',25, GalleryPage('School Trip')),
            _buildCard(AssetImage('dashboard_background.png'), 'My Album',25, GalleryPage('My Album')),
          ],
        ),
      ),
    );
  }
}