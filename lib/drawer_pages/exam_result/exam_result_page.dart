import 'package:flutter/material.dart';

class ExamResultPage extends StatefulWidget{
  @override
  _ExamResultPageState createState() => _ExamResultPageState();
}

class _ExamResultPageState extends State<ExamResultPage> {

  TextEditingController _examTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _examTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _examTextController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;


    Container _proceedButton(){
      return Container(
        width: MediaQuery.of(context).size.width/1.3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.blueAccent,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [BoxShadow(
              color: Colors.black45,
              blurRadius: 5.0,
            ),]
        ),
        child: Text('Proceed',
          style: TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }


    return Scaffold(
      appBar: AppBar(
        title: Text('Exam Results',style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 1.0,
        iconTheme: IconThemeData(
            color: Colors.blue
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: TextField(
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                      controller: _examTextController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15.0),
                          labelText: 'Select Exam',
                          border: InputBorder.none
                      ),
                    ),
                  ),
                  Container(
                    height: 1.0,
                    color: Colors.grey,
                    margin: EdgeInsets.only(left: _width/25, right: _width/25),
                  ),

                  SizedBox(
                    height: MediaQuery.of(context).size.height/15,
                  ),
                  InkWell(
                    borderRadius: BorderRadius.circular(50.0),
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (BuildContext context) => ExamResultPage())
                      );
                    },
                    child: _proceedButton(),
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}