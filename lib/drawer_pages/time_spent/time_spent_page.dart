import 'package:flutter/material.dart';

class TimeSpentPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    Container _getInfo(String activity,String time){
      return Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(activity,
                style: TextStyle(
                    color: Colors.grey
                ),
              ),
              Text(time,
                style: TextStyle(
                    color: Colors.grey
                ),
              ),
            ],
          ),
        ),
      );
    }
    return Scaffold(
        appBar: AppBar(
          title: Text('Time Spent',
              style: TextStyle(color: Colors.black)),
          iconTheme: IconThemeData(
              color: Colors.blue
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 1.0,

          actions: <Widget>[
            FlatButton(onPressed: (){}, child: Text("Download PDF",
              style: TextStyle(
                  color: Colors.grey
              ),))
          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: _width/11, right: _width/11),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Last features used",
                      ),
                      Text("Total Time")
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("E - Learning",
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      Text("00:40:00",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 1.0,
                  color: Colors.grey,
                ),
                _getInfo("News Feed", "00:40:00"),
                _getInfo("Section Of interest", "00:40:00"),
                _getInfo("Attendance", "00:40:00"),
                _getInfo("Time table", "00:40:00"),
                _getInfo("Assignment", "00:40:00"),
                _getInfo("Chat box", "00:40:00"),
              ],
            ),
          ),
        ),
    );
  }
}