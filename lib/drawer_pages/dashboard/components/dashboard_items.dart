import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/attendance/attendance_page.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/chat_box_page.dart';
import 'package:iviewsmart_student/pages/fees_payment_page/fees_payment_page.dart';
import 'package:iviewsmart_student/pages/leave_management_pages/leave_management_page.dart';
import 'package:iviewsmart_student/pages/smart_tips_page.dart';
import 'package:iviewsmart_student/pages/student_insurance_page.dart';
import 'package:iviewsmart_student/pages/query/student_queries_page.dart';
import 'package:iviewsmart_student/pages/time_table/time_table_page.dart';

class DashboardItems extends StatefulWidget{
  @override
  _DashboardItemsState createState() => _DashboardItemsState();
}

class _DashboardItemsState extends State<DashboardItems> {

  _item(String text, String image, Object page){
    double containerHeight = MediaQuery.of(context).size.height/8.3;
    double containerWidth = MediaQuery.of(context).size.width/4.1;
    return InkWell(
      onTap: (){
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => page)
        );
      },
      child: Container(
        height: containerHeight,
        width: containerWidth,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: Image.asset(image,
                width: containerWidth/4,
                height:  containerHeight/3,
              ),
            ),
            Text(text,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black,fontSize: containerHeight/7),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/4,
      color: Colors.grey[200],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _item('Fees\nPayment','fees_payment.png',FeesPaymentPage(),),
              _item('Time\nTable','fees_payment.png', TimeTablePage(),),
              _item('Student\nQuries','student_query.png', StudentQueriesPage(),),
              _item('Atendance','attendance.png', AttendancePage(),),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _item('Smart\nTips','fees_payment.png', SmartTipsPage(),),
              _item('Stuent\nInsurance','student_insurance.png', StudentInsurancePage(),),
              _item('Leave\nManagemenet','fees_payment.png', LeaveManagementPage(),),
              _item('Chat Box','chat_box.png', ChatBoxPage(),),
            ],
          ),
        ],
      ),
    );
  }
}