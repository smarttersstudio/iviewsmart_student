import 'package:flutter/material.dart';
import 'package:iviewsmart_student/drawer_pages/gallery/album_page.dart';
import 'package:iviewsmart_student/drawer_pages/assignment/assignment_page.dart';
import 'package:iviewsmart_student/drawer_pages/dashboard/dash_board_page.dart';
import 'package:iviewsmart_student/drawer_pages/exam_result/exam_result_page.dart';
import 'package:iviewsmart_student/drawer_pages/holiday/holiday_list_page.dart';
import 'package:iviewsmart_student/drawer_pages/time_spent/time_spent_page.dart';
import 'package:iviewsmart_student/pages/login_page.dart';


class HomeDrawer extends StatefulWidget{

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  int _drawerSelectedIndex = 0;

  getDrawerItemWidget() {
    switch (_drawerSelectedIndex) {
      case 0:
        return DashBoardPage();
      default:
        return DashBoardPage();
    }
  }

  final drawerItems = [
    DrawerItem('Dashboard', Icons.home),
    DrawerItem('Assignment', Icons.work),
    DrawerItem('Holiday List', Icons.assignment),
    DrawerItem('Time Spent', Icons.access_time),
    DrawerItem('Exam Result',Icons.description),
    DrawerItem('Album',Icons.photo_library),
  ];

  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];
    for (var i=0;  i<drawerItems.length;  i++) {
      var d = drawerItems[i];
      drawerOptions.add(
        Container(
          height: 50.0,
          child: ListTile(
              leading:  Icon(d.icon),
              title:  Text(d.title, overflow: TextOverflow.ellipsis,),
              selected: i == _drawerSelectedIndex,
              onTap: (){
                setState(() {
                  _onSelectItem(i);
                });
              }
          ),
        ),
      );
    }
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                  color: Colors.white
              ),
              accountName: Text('Iviewsmart',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 22.0,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.blueAccent,
                radius: 50.0,
                backgroundImage: AssetImage('account_logo.png'),
              ),
              accountEmail: null,
              otherAccountsPictures: <Widget>[
                InkWell(
                  onTap: (){
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (BuildContext context) => LoginPage())
                    );
                  },
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    backgroundImage: AssetImage('signout.png'),
                  ),
                ),
              ],
            ),
            Column(
              children: drawerOptions,
            ),
          ],
        ),
      ),
    );
  }

  _onSelectItem(int index) {
    //setState(() => _drawerSelectedIndex = index);
    Navigator.of(context).pop(); // close the drawer
    switch (index) {
      case 1:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => AssignmentPage())
        );
        break;
      case 2:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => HolidayListPage())
        );
        break;
      case 3:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => TimeSpentPage())
        );
        break;
      case 4:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => ExamResultPage())
        );
        break;
      case 5:
        Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => AlbumPage())
        );
        break;
    }
    setState(() {
      _drawerSelectedIndex = 0;
    });
  }
}

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}