import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/notification_page.dart';
class DashboardAppbar extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height : MediaQuery.of(context).orientation == Orientation.portrait ? MediaQuery.of(context).size.height/3 : MediaQuery.of(context).size.width/2.5,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage('dashboard_background.png'), fit: BoxFit.fill),
          ),
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height/10,
              bottom: MediaQuery.of(context).size.height/20,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                backgroundColor: Colors.black45,
                radius: MediaQuery.of(context).size.width/12,
                backgroundImage: AssetImage('account_logo.png'),
              ),
              SizedBox(
                width: 20.0,
              ),
              RichText(
                text: TextSpan(
                  text: 'Rishab Shukla',
                  style: TextStyle(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(text: '\n'),
                    TextSpan(
                      style: TextStyle(color: Colors.white,fontSize: 12.0, height: 1.5, fontWeight: FontWeight.normal),
                      text: 'Enrolment ID : 123456789',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: 0.0,
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
          child: AppBar(
            backgroundColor: Colors.transparent,
            title: Text('Iviewsmart' ,style: TextStyle(fontSize: 18.0, color: Colors.white, fontWeight: FontWeight.bold),),
            centerTitle: true,
            elevation: 0.0,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.notifications,  color: Colors.white,),
                tooltip: 'Notification',
                onPressed: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (BuildContext context) => NotificationPage())
                  );
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}