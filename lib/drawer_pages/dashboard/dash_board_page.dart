import 'package:flutter/material.dart';
import 'package:iviewsmart_student/drawer_pages/dashboard/components/dashboard_items.dart';
import 'package:iviewsmart_student/drawer_pages/dashboard/components/home_drawer.dart';
import 'package:iviewsmart_student/drawer_pages/dashboard/components/dashboard_app_bar.dart';
import 'package:iviewsmart_student/ui_components/image_slider.dart';

class DashBoardPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: HomeDrawer(),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              DashboardAppbar(),
              DashboardItems(),
              SizedBox(
                height: MediaQuery.of(context).size.height/20,
              ),
              CarouselWithIndicator(),
              ],
            ),
        ),
      ),
    );
  }
}