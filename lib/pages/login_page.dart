import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iviewsmart_student/pages/main_page.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController _enrNumberController, _dobController;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _enrNumberController = TextEditingController();
    _dobController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _enrNumberController.dispose();
    _dobController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage('background.jpg'), fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Image(
                image: AssetImage('logo.png'),
                width: MediaQuery.of(context).size.width/3,
                height: MediaQuery.of(context).size.height/5,
              ),
             Card(
               elevation: 5.0,
               margin: EdgeInsets.symmetric(horizontal : MediaQuery.of(context).size.width*0.1),
               shape: RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(6.0),
               ),
               child:Container(
                 child: Padding(
                   padding: const EdgeInsets.all(20.0),
                   child: Column(
                     mainAxisSize: MainAxisSize.max,
                     mainAxisAlignment: MainAxisAlignment.spaceAround,
                     children: <Widget>[
                       Container(
                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(3.0),
                           shape: BoxShape.rectangle,
                           border: Border.all(
                               color: Colors.black38,
                               width: 1.0,
                               style: BorderStyle.solid,
                           )
                         ),
                         child: TextField(
                           controller: _enrNumberController,
                           decoration: InputDecoration(
                             contentPadding: EdgeInsets.all(15.0),
                             hintText: 'Enter your Enrolment Number',
                             border: InputBorder.none
                           ),
                         ),
                       ),
                       SizedBox(
                         height: 7.0,
                       ),
                       Container(
                         alignment: Alignment.centerLeft,
                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(3.0),
                             shape: BoxShape.rectangle,
                             border: Border.all(
                               color: Colors.black38,
                               width: 1.0,
                               style: BorderStyle.solid
                             ),
                         ),
                         child: DateTimePickerFormField(
                           controller: _dobController,
                           inputType: InputType.date,
                           editable: false,
                           format: DateFormat('dd/MM/yyyy'),
                           decoration: InputDecoration(
                             hintText: 'Enter Your date of Birth',
                             contentPadding: EdgeInsets.all(15.0),
                             border: InputBorder.none,
                           ),
                         ),
                       ),
                       SizedBox(
                         height: 15.0,
                       ),
                       InkWell(
                         borderRadius: BorderRadius.circular(50.0),
                         onTap: (){
                           Navigator.of(context).pushReplacement(
                               MaterialPageRoute(builder: (BuildContext context) => MainPage())
                           );
                         },
                         child: Container(
                           width: MediaQuery.of(context).size.width/1.3,
                           height: 50.0,
                           alignment: Alignment.center,
                           decoration: BoxDecoration(
                               color: Colors.blue,
                               borderRadius: BorderRadius.circular(50.0),
                               boxShadow: [BoxShadow(
                                 color: Colors.black45,
                                 blurRadius: 3.0,
                               ),]
                           ),
                           child: Text('Login',
                           style: Theme.of(context).textTheme.button,
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),
               ),
             )
            ],
          ),
        ),
      ),
    );
  }
}