import 'dart:async';

import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/login_page.dart';

class SplashScreen extends StatefulWidget{
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 1),
            () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (BuildContext context) => LoginPage())
              );
            }
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Image(
              image:  AssetImage('logo.png'),
              width: MediaQuery.of(context).size.width/3,
              height: MediaQuery.of(context).size.height/5,
            ),
//            Text('IVIEWSMART',
//              style: TextStyle(
//                fontSize: 28.0,
//                color: Colors.blueAccent,
//                fontWeight: FontWeight.bold,
//              ),
//            ),
            SizedBox(
              height: MediaQuery.of(context).size.height/6,
            ),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
            ),
          ],
        ),
      ),
    );
  }
}