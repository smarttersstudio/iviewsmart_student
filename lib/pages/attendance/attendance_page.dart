import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AttendancePage extends StatefulWidget{
  @override
  _AttendancePageState createState() => _AttendancePageState();
}

class _AttendancePageState extends State<AttendancePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _startDate, _endDate;
  int _radioValue;

  @override
  void initState() {
    _startDate = TextEditingController();
    _endDate = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _startDate.dispose();
    _endDate.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    Container _proceedButton(){
      return Container(
        width: MediaQuery.of(context).size.width/1.3,
        height: 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [BoxShadow(
              color: Colors.black45,
              blurRadius: 5.0,
            ),]
        ),
        child: Text('Proceed',
          style: TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),
        ),
      );
    }

    Widget _getDatePicker(String title, controller){

      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title),
          Wrap(
            direction: Axis.horizontal,
            spacing: 5.0,
            crossAxisAlignment: WrapCrossAlignment.end,
            children: <Widget>[
              Icon(Icons.calendar_today, size: _width/20,color: Colors.blue,),
              Wrap(
                direction: Axis.horizontal,
                crossAxisAlignment: WrapCrossAlignment.start,
                children: <Widget>[
                  Container(
                    width: 100.0,
                      height: 35.0,
                      alignment: Alignment.center,
                      child: DateTimePickerFormField(
                        controller: controller,
                        inputType: InputType.date,
                        editable: false,
                        format: DateFormat('dd/MM/yyyy'),
                        textAlign: TextAlign.start,

                        style: TextStyle(
                          fontSize: 12.0,
                          color: Colors.black54
                        ),
                      ),
                  ),
                ],
              ),
            ],
          ),
        ],
      );
    }

    void something(int e){
        setState(() {
          if (e == 1){
            _radioValue = 1;
          } else if (e == 2){
        _radioValue = 2;
        } else if (e == 3){
          _radioValue = 3;
        }
      });
    }

    Widget _getRadioButtons(){
      return Container(
        padding: EdgeInsets.all(_width/20),
        child: Wrap(
          spacing: 5.0,
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Wrap(
              alignment: WrapAlignment.center,
              spacing: -5.0,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Radio(
                  onChanged: (int e) => something(e),
                  activeColor: Colors.blue,
                  value: 1,
                  groupValue: _radioValue,
                ),
                Text(
                  'Last 7 days',
                  style: new TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),

              ],
            ),
            Wrap(
              alignment: WrapAlignment.center,
              spacing: -5.0,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Radio(
                  onChanged: (int e) => something(e),
                  activeColor: Colors.blue,
                  value: 2,
                  groupValue: _radioValue,
                ),
                Text(
                  'Last 15 days',
                  style: new TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),

              ],
            ),
            Wrap(
              alignment: WrapAlignment.center,
              spacing: -5.0,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                Radio(
                  onChanged: (int e) => something(e),
                  activeColor: Colors.blue,
                  value: 3,
                  groupValue: _radioValue,
                ),
                Text(
                  'Last 1 month',
                  style: new TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Attendance',
          style: TextStyle(
              color: Colors.black,
          ),
        ),
        elevation: 1.0,
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blue,
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(top: _width/11),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _getDatePicker('From', _startDate),
                _getDatePicker('To', _endDate),
              ],
            ),
            SizedBox(
              height: _height/20,
            ),
            Text("OR",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
//            SizedBox(
//              height: _height/40,
//            ),
            _getRadioButtons(),
//            SizedBox(
//              height: _height/20,
//            ),
            _proceedButton(),
          ] ,
        ),
      )
    );
  }
}