import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SmartTipsPage extends StatefulWidget{
  @override
  _SmartTipsPageState createState() => _SmartTipsPageState();
}

class _SmartTipsPageState extends State<SmartTipsPage> {


  void handleResponse(response, {String appName}) {
    if (response == 0) {
      print("failed.");
    } else if (response == 1) {
      print("success");
    } else if (response == 2) {
      print("application isn't installed");
      if (appName != null) {
        _scaffoldKey.currentState.showSnackBar(new SnackBar(
          // ignore: unnecessary_brace_in_string_interps
          content: new Text("${appName} isn't installed."),
          duration: new Duration(seconds: 4),
        ));
      }
    }
  }

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    _buildCard(String text) {
    var _cardHeight = MediaQuery.of(context).size.height/5;
    var _cardWidth = MediaQuery.of(context).size.width/1.15;

    return Card(
      elevation: 2.5,
      margin: EdgeInsets.only(top : 10.0, left: 20.0, right: 20.0),
      child: Container(
        //width: _cardWidth,
        //height: _cardHeight,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100.0),

        ),
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              //height: _cardHeight/1.8,
              width: _cardWidth/1.031,
              alignment: Alignment.topCenter,
              child: Text(text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: _cardHeight/10,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
            Container(
              height: 0.5,
              color: Colors.grey,
              margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
            ),
            Container(
              height: _cardHeight/3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    child: Container(
                      height: _cardHeight/5,
                      margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
                      padding: EdgeInsets.only(left :10.0, right: 10.0,top: 5.0, bottom: 5.0),
                      decoration: BoxDecoration(
                          color: Color(0xff095CD4),
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [BoxShadow(
                            color: Colors.black45,
                            blurRadius: 3.0,
                            offset: Offset(0.5, 0.5)
                          ),]
                      ),
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.share,
                            color: Colors.white,
                            size: _cardHeight/10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left : 5.0, right: 5.0),
                            child: Text('Share',
                              style: TextStyle(
                                fontSize: _cardHeight/13,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: (){

                    },
                  ),
                  InkWell(
                    onTap: () async {
                     /* AdvancedShare.generic(
                        msg: "Its good.",
                        title: "Share with Advanced Share",
                      ).then((response){
                        print(response);
                      });

                      AdvancedShare.whatsapp(msg: "It's okay :)")
                          .then((response) {
                        handleResponse(response, appName: "Whatsapp");
                      });*/

                      var whatsappUrl ="whatsapp://send?phone=+91-9438560593";
                      await canLaunch(whatsappUrl)? launch(whatsappUrl):print("open whatsapp app link or do a snackbar with notification that there is no whatsapp installed");

                    },
                    child: Container(
                      height: _cardHeight/5,
                      margin: EdgeInsets.only(left: 5.0, right: 10.0, top: 5.0, bottom: 5.0),
                      padding: EdgeInsets.only(left :10.0, right: 10.0,top: 5.0, bottom: 5.0),
                      decoration: BoxDecoration(
                          color: Color(0xff2CB742),
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [BoxShadow(
                              color: Colors.black45,
                              blurRadius: 3.0,
                              offset: Offset(0.5, 0.5),
                          ),]
                      ),
                      child: Row(
                        children: <Widget>[
                          Image.asset('whatsapp_logo.png',
                            alignment: Alignment.center,
                            width: _cardHeight/10,
                            height: _cardHeight/10,
                            color: Colors.white,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left : 3.0),
                            child: Text('Share on Whatsapp',
                              style: TextStyle(
                                fontSize: _cardHeight/13,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Smart Tips', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(bottom: 10.0),
          color: Color(0xffEDEBEC),
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildCard('Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test. Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test.'),
              _buildCard('Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test.'),
              _buildCard('Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test.'),
              _buildCard('Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test.'),
              _buildCard('Understand Your Goals. You have to ask why you\'r going to invest the time and energy to do well on a test.'),
            ],
          ),
        ),
      ),
    );
  }
}
