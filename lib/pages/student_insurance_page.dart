import 'package:flutter/material.dart';

class StudentInsurancePage extends StatefulWidget{
  @override
  _StudentInsurancePageState createState() => _StudentInsurancePageState();
}

class _StudentInsurancePageState extends State<StudentInsurancePage> {

  TextEditingController _nameController = TextEditingController(text: 'Rishab Shukla');

  @override
  void initState() {
    super.initState();
    _nameController.text = 'Rishab Shukla';
    //_nameController = TextEditingController(text: 'Rishab Shukla');
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    _buildField(String hint, String text){
      return Container(
        height: _height/15,
        margin: EdgeInsets.only(top:20.0, left:20.0, right: 20.0),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(hint,
              style: TextStyle(color: Colors.grey,fontSize: 14.0,),
            ),
            Text(text,
              style: TextStyle(color: Colors.black,fontSize: 16.0,fontWeight: FontWeight.bold ),),
            Container(
              height: 1.0,
              color: Colors.grey,
              width: _width,
              margin: EdgeInsets.only(left: 20.0, right: 20.0),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Student Insurance', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: Container(
        height: _height,
        width: _width,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _buildField('Enter Your Name','Rishabh Shukla'),
                _buildField('Your date of Birth','25/09/1997'),
              ],
            ),
            Container(
              width: _width,
              height: _height/12,
              margin: EdgeInsets.all(_width/17),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.blueAccent,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [BoxShadow(
                    color: Colors.black45,
                    blurRadius: 5.0,
                  ),]
              ),
              child: GestureDetector(
                onTap: (){

                },
                child: Text('Proceed',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}