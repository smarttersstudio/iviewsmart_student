import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/new_add_subject_page.dart';
import 'package:iviewsmart_student/ui_components/editable_circle_image.dart';

class NewAddParticipantPage extends StatefulWidget {
  @override
  _NewAddParticipantPageState createState() => _NewAddParticipantPageState();
}

class _NewAddParticipantPageState extends State<NewAddParticipantPage> {

  List<String> items = [];

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;
    var _height = MediaQuery.of(context).size.height;

    _selectedItems(){
      ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: items.length,
        itemBuilder: (BuildContext context, index) {
//          return items.length == 0 ? Wrap(
//            direction: Axis.vertical,
//            crossAxisAlignment: WrapCrossAlignment.center,
//            children: <Widget>[
//              EditableCircleImage(
//                icon: Icons.close,
//                backgroundImage: AssetImage('account_logo.png'),
//                onPressed: (){
//
//                },
//                size: _width/6,
//              ),
//              SizedBox(height: 4.0,),
//              Text(items[index],
//                style: TextStyle(
//                  fontSize: 10.0,
//                  color: Colors.black87,
//                ),
//              ),
//            ],
//          ): Container();
        });
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.only(top :10.0, left: _width/25, right: _width/25),
        child: Wrap(
          direction: Axis.horizontal,
          spacing: 8.0,
          children: <Widget>[
            Wrap(
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: <Widget>[
                EditableCircleImage(
                  icon: Icons.close,
                  backgroundImage: AssetImage('account_logo.png'),
                  onPressed: (){

                  },
                  size: _width/6,
                ),
                SizedBox(height: 4.0,),
                Text('+91 9876543210',
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                EditableCircleImage(
                  icon: Icons.close,
                  backgroundImage: AssetImage('account_logo.png'),
                  onPressed: (){

                  },
                  size: _width/6,
                ),
                SizedBox(height: 4.0,),
                Text('+91 9876543210',
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                EditableCircleImage(
                  icon: Icons.close,
                  backgroundImage: AssetImage('account_logo.png'),
                  onPressed: (){

                  },
                  size: _width/6,
                ),
                SizedBox(height: 4.0,),
                Text('+91 9876543210',
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                EditableCircleImage(
                  icon: Icons.close,
                  backgroundImage: AssetImage('account_logo.png'),
                  onPressed: (){

                  },
                  size: _width/6,
                ),
                SizedBox(height: 4.0,),
                Text('+91 9876543210',
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                EditableCircleImage(
                  icon: Icons.close,
                  backgroundImage: AssetImage('account_logo.png'),
                  onPressed: (){

                  },
                  size: _width/6,
                ),
                SizedBox(height: 4.0,),
                Text('+91 9876543210',
                  style: TextStyle(
                    fontSize: 10.0,
                    color: Colors.black87,
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }

    _buildCard(String name, {String status, ImageProvider image}) {
      return ListTile(
        leading: CircleAvatar(
          backgroundImage: image,
          radius: 25,
        ),
        title: Text(name,
          style: TextStyle(color: Colors.black, fontSize: 14.0,),
        ),
        subtitle: status != null ? Text('($status)',
          style: TextStyle(color: Colors.black54, fontSize: 10.0,),
        ) : null,
        onTap: () {
          setState(() {
            items.add('+91 9438295102');
            print(items.toString());
          });
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        title: RichText(
          text: TextSpan(
            text: 'New Group',
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.black,
            ),
            children: [
              TextSpan(
                  text: '\n'
              ),
              TextSpan(
                text: 'Add Participants',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12.0
                ),
              )
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey,),
            onPressed: (){

            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => NewAddSubject())
            );
          },
          icon: Icon(Icons.arrow_forward),
          label: Text('Next'),
        elevation: 3.0,
        heroTag: 'fabTag',
      ),

      body: ListView(
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        children: <Widget>[
          _selectedItems(),

          Divider(indent: _width/25,),

          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
        ],
      ),
//      ListView.builder(
//        itemCount: 20,
//        itemBuilder: (BuildContext context, index) => _buildCard('Name'),
//        padding: const EdgeInsets.only(top:10.0, bottom: 10.0),
//      ),
    );
  }
}
