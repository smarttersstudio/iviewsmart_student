import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/chat_box_page.dart';

class NewAddSubject extends StatefulWidget {
  @override
  _NewAddSubjectState createState() => _NewAddSubjectState();
}

class _NewAddSubjectState extends State<NewAddSubject> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        title: RichText(
          text: TextSpan(
            text: 'New Group',
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.black,
            ),
            children: [
              TextSpan(
                  text: '\n'
              ),
              TextSpan(
                text: 'Add Subject',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12.0
                ),
              )
            ],
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey,),
            onPressed: (){

            },
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Positioned(
            top: 0.0,
            left: 0.0,
            right: 0.0,
            height: _height/4.5,
            child: Container(
              color: Colors.white,
              child: Wrap(
                direction: Axis.vertical,
                children: <Widget>[
                  Row(
                    //direction: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        child: IconButton(
                          icon: Icon(Icons.photo_camera, color: Colors.blue,),
                          onPressed: (){

                          },
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(_width/2),
                          color: Colors.grey.withOpacity(0.3),
                        ),
                        margin: EdgeInsets.all(_width/25),
                      ),
                      Container(
                        width: _width/1.7,
                        margin: EdgeInsets.only(bottom: _width/25),
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: 'Type group subject here...',
                            hintStyle: TextStyle(fontSize: _width/20),
                            alignLabelWithHint: true,
                          ),
                          style: TextStyle(
                            fontSize: _width/20,
                            color: Colors.black
                          ),

                        ),
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left:_width/20),
                    child: Text('Provide a group subject and optional group icon.',
                    style: TextStyle(fontSize: 10.0, color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: _height/5.5,
            right: 10.0,
            child: FloatingActionButton.extended(
              onPressed: (){
                var route = MaterialPageRoute(builder: (BuildContext context) => ChatBoxPage());
                Navigator.of(context).pushAndRemoveUntil(route, (Route<dynamic> route){
                  return route.isFirst;
                });
              },
              icon: Icon(Icons.done),
              label: Text('Done'),
              elevation: 3.0,
              heroTag: 'fabTag',
            ),
          ),
        ],
      ),
    );
  }
}
