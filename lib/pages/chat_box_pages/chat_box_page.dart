import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/personal_chat_page.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/select_people_page.dart';
import 'package:iviewsmart_student/utils/data_lists.dart';

class ChatBoxPage extends StatefulWidget{
  @override
  _ChatBoxPageState createState() => _ChatBoxPageState();
}

class _ChatBoxPageState extends State<ChatBoxPage> {
  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    _buildCard(ImageProvider backgroundImage, String title, String subtitle, String time, index){
      return Container(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: Wrap(
          children: <Widget>[
            ListTile(
              leading: CircleAvatar(
                backgroundImage: backgroundImage,
                radius: 25,
              ),
              title: Text(title,
                style: TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              subtitle: Wrap(
                  children: <Widget>[
                    Icon(Icons.done_all, color: Colors.blueAccent,size: 18.0,),
                    SizedBox(width: 5.0,),
                    Text(subtitle,
                      style: TextStyle(color: Colors.grey, fontSize: 14.0,),
                    ),
                  ]),
              trailing: Container(
                  margin: EdgeInsets.only(top: 5.0, bottom: 20.0),
                  child: Text(time,
                    style: TextStyle(color: Colors.grey, fontSize: 14.0),
                  ),
              ),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => PersonalChatPage(title: title,type: index == 0 || index == 3 || index == 6 ? 'Group': 'other',))
                );
              },
            ),
            Container(
              margin: EdgeInsets.only(left: _width/4, right: _width/40),
              height: 1.0,
              width: _width/1.2,
              color: Colors.grey.withOpacity(0.5),
            ),
          ],
        ),
      );
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => SelectPeoplePage())
          );
        },
        heroTag: 'fabTag',
        elevation: 4.0,
        foregroundColor: Colors.blueAccent,
        child: Icon(Icons.person_add, color: Colors.white,size: 30.0,),
      ),
      appBar: AppBar(
        title: Text('Chat Box', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 4.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: ListView.builder(
        itemCount: Items.accountImageList.length,
        itemBuilder: (BuildContext context, index) => _buildCard(AssetImage(Items.accountImageList[index]), Items.chatContactName[index],Items.chatData[index],Items.chatTime[index], index),
        padding: const EdgeInsets.only(top:10.0, bottom: 10.0),
      ),
    );
  }
}