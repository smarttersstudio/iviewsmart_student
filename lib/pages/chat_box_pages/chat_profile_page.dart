import 'package:flutter/material.dart';

class ChatProfilePage extends StatefulWidget{
  final String _profileName;

  ChatProfilePage(this._profileName);

  @override
  _ChatProfilePageState createState() => _ChatProfilePageState();
}

class _ChatProfilePageState extends State<ChatProfilePage> {

  ScrollController _scrollController;

  @override
  void initState() {

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    _scrollController = ScrollController(initialScrollOffset: _height/3);

    _muteNotification(){
      bool _isSwitched = true;
      return Container(
        color: Colors.white,
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.only(top: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Mute Notifications',
              style: TextStyle(
                fontSize: _width/18,
                color: Colors.black,
              ),
            ),
            Switch(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onChanged: (value){
                setState(() {
                  _isSwitched = value;
                });
              },
              value: _isSwitched,
            ),
          ],
        ),
      );
    }
    _mediaCard(){
      return Container(
        margin: EdgeInsets.only(top: 8.0),
        padding: EdgeInsets.all(10.0),
        width: _width,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Media',
                style: TextStyle(color: Colors.blue),),
                Text('3'),
              ],
            ),
            Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 5.0, right: 3.0),
                  height: _width/5,
                  width: _width/5,
                  color: Colors.pinkAccent,
                ),
                Container(
                  margin: EdgeInsets.only(top: 5.0, right: 3.0),
                  height: _width/5,
                  width: _width/5,
                  color: Colors.pink,
                ),
                Container(
                  margin: EdgeInsets.only(top: 5.0, right: 3.0),
                  height: _width/5,
                  width: _width/5,
                  color: Colors.red,
                ),
              ],
            ),
          ],
        ),
      );
    }

    _participant(bool isAdmin, String name, String status){
      return ListTile(
        leading: CircleAvatar(
          //backgroundImage: backgroundImage,
          backgroundColor: Colors.blue,
          radius: 25,
        ),
        title: Text(name,
          style: TextStyle(color: Colors.black, fontSize: _width/25,),
        ),
        subtitle: !isAdmin ? Text(status,
          style: TextStyle(color: Colors.grey, fontSize: _width/30,),
        ) : null,
        trailing: isAdmin ? Container(
          padding: EdgeInsets.all(3.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3.0),
            border: Border.all(color: Colors.blue, width: 1.0, style: BorderStyle.solid)
          ),
          //margin: EdgeInsets.only(top: 5.0, bottom: 20.0),
          child: Text('Group Admin',
            style: TextStyle(color: Colors.blue, fontSize: 9.0),
          ),
        ): null,
        onTap: () {

        },
      );
    }

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: _height/1.5,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Wrap(
                  direction: Axis.vertical,
                  children: <Widget>[
                    Text(widget._profileName,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    )),
                    Text('Created: 13/01/19',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8.0,
                      )),
                    ],
                ),
                background: Container(
                  width: _width,
                  color: Color(0xff8CD8F2),
                ),
//                Image.asset(
//                  'dashboard_background.png',
//                  fit: BoxFit.cover,
//                ),
              ),
            ),
          ];
        },
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _mediaCard(),
              _muteNotification(),
              _participant(true, 'School Admin', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
              _participant(false, 'Rishabh Shukla', 'Hi there! I am using whatsapp'),
            ],
          ),
        ),
      ),
    );
  }
}
