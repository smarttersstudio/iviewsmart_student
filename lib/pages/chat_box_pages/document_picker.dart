import 'dart:io';

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class DocumentPicker extends StatefulWidget {
  @override
  _DocumentPickerState createState() => _DocumentPickerState();
}

class _DocumentPickerState extends State<DocumentPicker> {

  File _image, _video, _document;

  Future<bool> _checkPermission() async {
    if (Theme.of(context).platform == TargetPlatform.android) {
      PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);
      if (permission != PermissionStatus.granted) {
        Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
        if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
          return true;
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;
    var _height = MediaQuery.of(context).size.height;

    _buildColumn(String text, Function onClick, IconData icon, Color bgColor){
      return InkWell(
        onTap: onClick,
        child: Column(
          children: <Widget>[
            Material(
              color: bgColor,
              type: MaterialType.circle,
              child: Padding(
                padding: EdgeInsets.all(_width/30),
                  child: Icon(icon, size: _width/10, color: Colors.white,),
              ),
            ),
            Text(text,
            style: TextStyle(fontSize: 16.0, color: Colors.black54, height: 1.5),
            ),
          ],
        ),
      );
    }
    return Container(
      height: _height/4,
      width: _width,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      alignment: Alignment.center,
      child: Wrap(
        direction: Axis.horizontal,
        spacing: _width/10,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: <Widget>[
          _buildColumn('Document',
              () async {
                if( await _checkPermission()) {
                  _document = await FilePicker.getFile(type: FileType.ANY).whenComplete((){
                    Navigator.of(context).pop();
                  });
                }
              },
              Icons.insert_drive_file,
              Colors.orange),
          _buildColumn('Image',
              ()async {
                if( await _checkPermission()) {
                  _image = await FilePicker.getFile(type: FileType.IMAGE).whenComplete((){
                    Navigator.of(context).pop();
                  });
                }
              },
              Icons.insert_photo,
              Colors.blue),
          _buildColumn('Video',
              ()async{
                if( await _checkPermission()) {
                  _video = await FilePicker.getFile(type: FileType.VIDEO).whenComplete((){
                    Navigator.of(context).pop();
                  });
                }
              },
              Icons.videocam,
              Colors.red),
        ],
      ),
    );
  }
}
