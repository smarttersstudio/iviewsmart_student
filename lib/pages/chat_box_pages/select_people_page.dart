import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/new_add_participant_page.dart';

class SelectPeoplePage extends StatefulWidget {
  @override
  _SelectPeoplePageState createState() => _SelectPeoplePageState();
}

class _SelectPeoplePageState extends State<SelectPeoplePage> {

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    _newGroupCard(){
      return ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.grey,
          radius: 25,
          child: Icon(Icons.people, color: Colors.white,size: _height/22,),
        ),
        title: Text('New Gruop',
            style: TextStyle(color: Colors.black, fontSize: 14.0,
          ),
        ),
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => NewAddParticipantPage()),
          );
        },
      );
    }
    _buildCard(String name, {String status, ImageProvider image}) {
      return ListTile(
        leading: CircleAvatar(
          backgroundImage: image,
          radius: 25,
        ),
        title: Text(name,
          style: TextStyle(color: Colors.black, fontSize: 14.0,),
        ),
        subtitle: status != null ? Text('($status)',
          style: TextStyle(color: Colors.black54, fontSize: 10.0,),
        ) : Text(''),
        onTap: () {

        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        title: RichText(
            text: TextSpan(
              text: 'Select People',
              style: TextStyle(
                fontSize: 14.0,
                  color: Colors.black,
              ),
              children: [
                TextSpan(
                  text: '\n'
                ),
                TextSpan(
                  text: '492 People',
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12.0
                  ),
                )
              ],
            ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.grey,),
            onPressed: (){

            },
          ),
          IconButton(
            icon: Icon(Icons.more_vert,),
            onPressed: (){

            },
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
        children: <Widget>[
          _newGroupCard(),
          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
          _buildCard('Anjali Sharma', status: 'Teacher' ),
          _buildCard('Aman Mishra', status: 'Teacher'),
          _buildCard('Neha Sharma'),
          _buildCard('Rahul Mishra'),
        ],
      ),
//      ListView.builder(
//        itemCount: 20,
//        itemBuilder: (BuildContext context, index) => _buildCard('Name'),
//        padding: const EdgeInsets.only(top:10.0, bottom: 10.0),
//      ),
    );
  }
}
