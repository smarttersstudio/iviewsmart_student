import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/chat_profile_page.dart';
import 'package:iviewsmart_student/pages/chat_box_pages/document_picker.dart';
import 'package:iviewsmart_student/ui_components/message_cards.dart';

class PersonalChatPage extends StatefulWidget{

  final String title;
  final String type;

  PersonalChatPage({this.title, this.type});

  @override
  _PersonalChatPageState createState() => _PersonalChatPageState();
}

class _PersonalChatPageState extends State<PersonalChatPage> {
  ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener((onFocusChange));
  }

  @override
  void dispose() {
    focusNode.dispose();
    listScrollController.dispose();
    super.dispose();
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      setState(() {
        listScrollController.animateTo(listScrollController.position.maxScrollExtent, curve: Curves.easeOut, duration: Duration(microseconds: 1));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var _width = MediaQuery.of(context).size.width;
    var _height = MediaQuery.of(context).size.height;

    _messageBox(){
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Material(
              type: MaterialType.circle,
              color: Colors.transparent,
              child: IconButton(
                color: Colors.transparent,
                icon: Icon(Icons.attachment,size: _width/12, color: Colors.grey,),
                onPressed: (){
                  showModalBottomSheet(
                      context: context,
                      builder: (BuildContext bc){
                        return DocumentPicker();
                      }
                  );
                },
              ),
            ),
            Flexible(
              child: Container(
                constraints: BoxConstraints(
                    maxHeight: _height/4,
                    minWidth: _width/1.6,
                    maxWidth: _width/1.6,
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  child: TextField(
                    maxLines: null,
                    focusNode: focusNode,
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    //controller: textEditingController,
                    decoration: InputDecoration.collapsed(
                      hintText: 'Type your message...',
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
              ),
            ),
            // Button send message
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Material(
                color: Colors.blue,
                type: MaterialType.circle,
                child: InkWell(
                  onTap: (){
                    listScrollController.jumpTo(listScrollController.position.maxScrollExtent);
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.send, size:_width/15, color: Colors.white,
                      ),
                  ),
                ),
              ),
            ),
          ],
        ),
        width: double.infinity,
        color: Colors.white,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: InkWell(
          splashColor: Colors.black12.withOpacity(0.1),
          onTap: (){
            Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context) => ChatProfilePage(widget.title))
            );
          },
          child: Container(
            height: _height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('account_logo.png'),
                    radius: 20,
                  ),
                ),
                Wrap(
                  direction: Axis.vertical,
                  children: <Widget>[
                    Text(widget.title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.0,
                      ),
                    ),
                    Text('89 People',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12.0,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        elevation: 3.0,
        iconTheme: IconThemeData(color: Colors.blue,),
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_vert,),
            onPressed: (){

            },
          ),
        ],
      ),

      body: Material(
        color: Colors.grey.withOpacity(0.2),
        child: Column(
          children: <Widget>[
            Flexible(
              child: ListView(
                controller: listScrollController,
                children: <Widget>[
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHello HelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HellooHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','Helo'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','Hello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HellloHello'),
                 widget.type == 'Group' ? GroupSenderCard('3:00', 'Hii') : PersonalSenderCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupSenderCard('3:00', 'Hii') : PersonalSenderCard('3:00','HelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupSenderCard('3:00', 'Hii') : PersonalSenderCard('3:00','HelloHe\nlloHelloHeHelloHello\nHelloHelloHellolloHello'),
                 widget.type == 'Group' ? GroupSenderCard('3:00', 'Hii') : PersonalSenderCard('3:00','HelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHelloH\n\n\n\nelloHelloHelloHelloHello'),
                 widget.type == 'Group' ? GroupReceiverCard('Vimal', '3:00', 'HelloHelloHelloHelloHello'):PersonalReceiverCard('3:00','HelloHelloHelloHelloHello'),
               ],
              ),
            ),
            _messageBox(),
          ],
        ),
      ),
    );
  }
}