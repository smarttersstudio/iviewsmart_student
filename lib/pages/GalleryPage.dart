import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/image_dialog.dart';

class GalleryPage extends StatefulWidget{

  final String title;

  GalleryPage(this.title);

  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: Colors.black),),
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.blue),
        elevation: 0.0,
      ),
      body: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: 7,
          padding: EdgeInsets.all(2.0),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 2.0,
              mainAxisSpacing: 2.0,
              crossAxisCount: 3
          ),
          itemBuilder: (context, index) =>
            GestureDetector(
              onTap: (){
                Navigator.of(context).push(new MaterialPageRoute<Null>(
                    builder: (BuildContext context) {
                      return ImageDialog('dashboard_background.png', index);
                    },
                    fullscreenDialog: true
                ));
              },
              child: Image.asset(
                  'dashboard_background.png',
                  fit: BoxFit.fill
              ),
            ),
        ),
    );
  }
}