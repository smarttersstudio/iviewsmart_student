import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/message_cards.dart';

class ChatPage extends StatefulWidget {

  final String _teacherName;
  final String _text;

  ChatPage(this._teacherName, this._text);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController textEditingController = TextEditingController();
  ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    textEditingController.value = TextEditingValue(text: widget._text);
    focusNode.addListener((onFocusChange));
  }

  @override
  void dispose() {
    focusNode.dispose();
    textEditingController.dispose();
    listScrollController.dispose();
    super.dispose();
  }

  void onFocusChange() {
    if (focusNode.hasFocus) {
      setState(() {
        listScrollController.animateTo(listScrollController.position.maxScrollExtent, curve: Curves.easeOut, duration: Duration(microseconds: 1));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
  var _height = MediaQuery.of(context).size.height;
  var _width = MediaQuery.of(context).size.width;
  _messageBox(){
    return Flexible(
      flex: 0,
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Flexible(
              child: Container(
                constraints: BoxConstraints(
                  maxHeight: _height/4,
                  minWidth: _width/1.6,
                  maxWidth: _width/1.6,
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  child: TextField(
                    maxLines: null,
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    controller: textEditingController,
                    decoration: InputDecoration.collapsed(
                      hintText: 'Type your message...',
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
              ),
            ),

            // Button send message
            Container(
              padding: const EdgeInsets.all(5.0),
              child: FloatingActionButton(
                elevation: 0.0,
                highlightElevation: 0.0,
                onPressed: (){

                },
                backgroundColor: Colors.blueAccent,
                child: Icon(Icons.send, size:_width/12),
              ),
            ),
          ],
        ),
        width: double.infinity,
        color: Colors.white,
      ),
    );
  }


  return Scaffold(
    appBar: AppBar(
      title: Text(widget._teacherName, style: TextStyle(color: Colors.black),),
      centerTitle: true,
      elevation: 2.0,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(
        color: Colors.blueAccent,
      ),
    ),
    body:Material(
      color: Colors.grey.withOpacity(0.2),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            child: ListView(
              controller: listScrollController,
              children: <Widget>[
                PersonalSenderCard('3:00',widget._text),
                PersonalReceiverCard('3:15', 'Ok'),
              ],
            ),
          ),
          _messageBox(),
        ]
      )
    )
    );
  }
}
