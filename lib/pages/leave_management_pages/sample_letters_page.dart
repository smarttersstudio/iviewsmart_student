import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/leave_management_pages/chat_page.dart';
import 'package:iviewsmart_student/utils/data_lists.dart';

class SampleLettersPage extends StatefulWidget {

  final String _teacherName;

  SampleLettersPage(this._teacherName);

  @override
  _SampleLettersPageState createState() => _SampleLettersPageState();
}

class _SampleLettersPageState extends State<SampleLettersPage> {
  @override
  Widget build(BuildContext context) {

    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('leave Managment', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: Container(
        height: _height,
        width: _width,
        color: Colors.grey.withOpacity(0.1),
        child: GridView.builder(
          itemCount: Items.sampleLetters.length,
            padding: EdgeInsets.all(_width/20),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: _width/20,
                mainAxisSpacing: _width/20,
            ),
            itemBuilder: (context, index){
              return GestureDetector(
                onTap: (){
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (BuildContext context) => ChatPage(widget._teacherName, Items.sampleLetters[index]))
                  );
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      height: _height/5,
                      child: Card(
                        elevation: 4.0,
                        child: Container(
                          padding: EdgeInsets.all(10.0),
                            //height: _height/4,
                            child: Text(
                                Items.sampleLetters[index],
                              style: TextStyle(
                                fontSize: _width/60,

                              ),
                            ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Text('Sample Letter ${index+1}',
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
        ),
      )
    );
  }
}
