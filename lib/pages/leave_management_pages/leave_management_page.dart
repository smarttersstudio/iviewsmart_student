import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/leave_management_pages/sample_letters_page.dart';

class LeaveManagementPage extends StatefulWidget{
  @override
  _LeaveManagementPageState createState() => _LeaveManagementPageState();
}

class _LeaveManagementPageState extends State<LeaveManagementPage> {

  String _teacherName;


  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
      title: Text('leave Managment', style: TextStyle(color: Colors.black),),
      centerTitle: true,
      elevation: 2.0,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(
        color: Colors.blueAccent,
      ),
    ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(_width/20),
              child: TextField(
                onChanged: (text){
                  setState(() {
                    text = 'R C Bansal';
                    _teacherName = text;
                  });
                },
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: _width/20),
                decoration: InputDecoration(
                  labelText: 'Select your Subject Teacher',
                  labelStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.normal),
                ),
              ),
            ),
            GestureDetector(
              onTap: (){
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (BuildContext context) => SampleLettersPage('R C Bansal'))
                );

              },
              child: Container(
                margin: EdgeInsets.all(_height/25,),
                width: _width,
                height: _height/11,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text('Proceed',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}