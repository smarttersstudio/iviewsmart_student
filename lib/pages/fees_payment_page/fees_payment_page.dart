import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/fees_payment_page/payment_successful_page.dart';

class FeesPaymentPage extends StatefulWidget{
  @override
  _FeesPaymentPageState createState() => _FeesPaymentPageState();
}

class _FeesPaymentPageState extends State<FeesPaymentPage> {

  TextEditingController _locationTextController,
      _schoolNameTextController,_enrNumberTextController,
      _nameTextController,_classTextController,
      _sectionTextController,_feesDueTextController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _locationTextController = TextEditingController();
    _schoolNameTextController = TextEditingController();
    _enrNumberTextController = TextEditingController();
    _nameTextController = TextEditingController();
    _sectionTextController = TextEditingController();
    _feesDueTextController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _locationTextController.dispose();
    _schoolNameTextController.dispose();
    _enrNumberTextController.dispose();
    _nameTextController.dispose();
    _sectionTextController.dispose();
    _feesDueTextController.dispose();
  }

  Container _proceedButton(){
    return Container(
      width: MediaQuery.of(context).size.width/1.3,
      height: 50.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.blueAccent,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [BoxShadow(
            color: Colors.black45,
            blurRadius: 5.0,
          ),]
      ),
      child: Text('Proceed',
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontWeight: FontWeight.bold
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('School Fees', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            margin: EdgeInsets.only(left: _width/25, right: _width/25),
            child: Column(
              children: <Widget>[

                Container(
                  child: TextField(
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                    ),
                    controller: _locationTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Enter your school location',
                        border: InputBorder.none
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                Container(
                  child: TextField(
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                    ),
                    controller: _schoolNameTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'School name',
                        border: InputBorder.none
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                Container(
                  child: TextField(
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                    ),
                    controller: _enrNumberTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Enrollement number',
                        border: InputBorder.none
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),
                Container(
                  child: TextField(
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      color: Colors.grey
                    ),
                    controller: _nameTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Student Name',
                        border: InputBorder.none
                    ),
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                Container(
                  child: TextField(
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        color: Colors.grey
                    ),
                    controller: _classTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Class',
                        border: InputBorder.none
                    ),
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                Container(
                  child: TextField(
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        color: Colors.grey
                    ),
                    controller: _sectionTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Section',
                        border: InputBorder.none
                    ),
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                Container(
                  child: TextField(
                    keyboardType: TextInputType.number,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                        color: Colors.grey,
                    ),
                    controller: _feesDueTextController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(15.0),
                        labelText: 'Fees due',
                        border: InputBorder.none,
                    ),
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: Colors.grey,
                  ),
                ),

                SizedBox(
                  height: _height/15,
                ),

                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: InkWell(
                    borderRadius: BorderRadius.circular(50.0),
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute(builder: (BuildContext context) => PaymentStatusPage())
                      );
                    },
                    child: _proceedButton(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}