import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/main_page.dart';
class PaymentStatusPage extends StatefulWidget{
  @override
  _PaymentStatusPageState createState() => _PaymentStatusPageState();
}

class _PaymentStatusPageState extends State<PaymentStatusPage> {

  Container _backToHomeButton(){
    return Container(
      width: MediaQuery.of(context).size.width/1.3,
      height: 50.0,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.blueAccent,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [BoxShadow(
            color: Colors.black45,
            blurRadius: 5.0,
          ),]
      ),
      child: Text('Back to home',
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontWeight: FontWeight.bold
        ),
      ),
    );
  }

  Container _getDetails(String title, String value){
    return Container(
      margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text(title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 12.0
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width/8,
          ),
          Text(value,
            style: TextStyle(
                fontSize: 12.0,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Summary', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 1.0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            margin: EdgeInsets.only(top: _width/11,left: _width/11, right: _width/11),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'assets/payment_successful.png',
                  height: _height/7,
                ),
                SizedBox(
                  height: _height/40,
                ),
                Text("Your payment was successful !",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  height: _height/40,
                ),
                Container(
                  margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/40),
                  color: Colors.grey,
                  height: 1.0,
                  alignment: Alignment.center,
                ),
                Container(
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    direction: Axis.vertical,
                    alignment: WrapAlignment.spaceEvenly,
                    children: <Widget>[
                      _getDetails("School name", "   JKG School"),
                      _getDetails("Student name", "  Rishabh Shukla"),
                      _getDetails("Enrollment No.", "12345"),
                      _getDetails("Date and time", "  21/01/17 and 9:03am"),
                      _getDetails("Amount Paid", "   ₹12000"),
                      _getDetails("Transaction ID", "12345"),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/40),
                  color: Colors.grey,
                  height: 1.0,
                  alignment: Alignment.center,
                ),
                Icon(Icons.insert_drive_file,
                  color: Colors.blue,
                ),
                Container(
                  margin: EdgeInsets.only(top: _height/50),
                    child: Text("Download Transaction Reciept"),
                ),
                SizedBox(
                  height: _height/20,
                ),
                InkWell(
                  borderRadius: BorderRadius.circular(50.0),
                  onTap: (){
                    var route = MaterialPageRoute(builder: (BuildContext context) => MainPage());
                    Navigator.of(context).pushAndRemoveUntil(route, (Route<dynamic> route){
                      return false;
                    });
                  },
                  child: _backToHomeButton(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}