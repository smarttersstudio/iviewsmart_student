import 'package:flutter/material.dart';
import 'package:iviewsmart_student/drawer_pages/dashboard/dash_board_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/e_learning/elearning_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/section_of_interest/interest_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/news_feed/neews_feed_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/profile/profile_page.dart';

class MainPage extends StatefulWidget{

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  PageController _pageController;

  int bottomPageIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      bottomPageIndex = page;
    });
  }

  @override
  Widget build(BuildContext context) {

    _bottomNavigationBar(IconData icon, String title){
      return BottomNavigationBarItem(
        icon: Icon(icon,color: Colors.black45,),
        title: Text(title,style: TextStyle(color: Colors.black38),),
      );
    }

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items:[
          _bottomNavigationBar(Icons.home, 'Home'),
          _bottomNavigationBar(Icons.rss_feed, 'News Feed'),
          _bottomNavigationBar(Icons.computer, 'E-learning'),
          _bottomNavigationBar(Icons.ondemand_video, 'Interest'),
          _bottomNavigationBar(Icons.person, 'Profile'),
        ],
        onTap: (page){
          setState(() {
            _pageController.animateToPage(
              page,
              duration: Duration(milliseconds: 1),
              curve: Curves.easeIn,
            );
          });
        },
        currentIndex: bottomPageIndex,
      ),
      body: PageView(
        children: <Widget>[
          DashBoardPage(),
          NewsFeedPage(),
          ELearningPage(),
          InterestPage(),
          ProfilePage(),
        ],
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (page){
          setState(() {
            bottomPageIndex = page;
          });
        },
        ),
    );
  }
}