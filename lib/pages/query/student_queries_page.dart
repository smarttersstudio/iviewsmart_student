import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/query/add_query_page.dart';
import 'package:iviewsmart_student/pages/query/tabs/all_queries_page.dart';

class StudentQueriesPage extends StatefulWidget{
  @override
  _StudentQueriesPageState createState() => _StudentQueriesPageState();
}

class _StudentQueriesPageState extends State<StudentQueriesPage> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Student Queries', style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
          bottom: TabBar(tabs: [
            Tab(child: Text('All',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),
            ),
            Tab(child: Text('Open',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),
            ),
            Tab(child: Text('Closed',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),
            ),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => AddQueryPage())
              );
            },
          child: Icon(Icons.add),
          backgroundColor: Colors.blue,
        ),
        body: TabBarView(children: [
          AllQueriesPage(),
          AllQueriesPage(),
          AllQueriesPage(),
        ])
      ),
    );
  }
}