import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/time_table/components/daily_routine_page.dart';

class TimeTablePage extends StatefulWidget{
  @override
  _TimeTablePageState createState() => _TimeTablePageState();
}

class _TimeTablePageState extends State<TimeTablePage> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Time Table', style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
          bottom: TabBar(tabs: [
            Tab(child: Text('Mon',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),
            ),
            Tab(child: Text('Tue',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),
            ),
            Tab(child: Text('Wed',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),

            ),
            Tab(child: Text('Thu',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),

            ),
            Tab(child: Text('Fri',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),

            ),
            Tab(child: Text('Sat',
              style: TextStyle(color: Colors.black, fontSize: width/25,),
            ),

            ),

          ]),
        ),
        body: TabBarView(children: [
          DailyRoutine(),
          DailyRoutine(),
          DailyRoutine(),
          DailyRoutine(),
          DailyRoutine(),
          DailyRoutine(),
        ]),
      ),
    );
  }
}