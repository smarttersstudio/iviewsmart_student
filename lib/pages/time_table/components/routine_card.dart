import 'package:flutter/material.dart';
class RoutineCard extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Card(
      margin: EdgeInsets.only(left: 30.0,right: 30.0,top: 8.0,bottom: 8.0),
      child: Container(
        padding: EdgeInsets.only(left: 15,right: 15,top: 5,bottom: 5),

        child: Row(
          children: <Widget>[
            Wrap(
              direction: Axis.vertical,
              spacing: 5.0,
              children: <Widget>[
                Text(
                    'Subject',
                  style: TextStyle(fontWeight:FontWeight.bold),
                ),
                Text(
                    '11.00 AM - 2.30 PM',
                  style: TextStyle(
                      color: Colors.grey[500]
                )
                ),
              ],
            ),
            Text(
                '1st Period',
              style: TextStyle(
                color: Colors.grey[500]
              ),
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ),
      ),
    );
  }

}