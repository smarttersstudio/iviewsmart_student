import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/time_table/components/routine_card.dart';
class DailyRoutine extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 10),
          color: Color.fromARGB(255, 247, 247, 247),
          child: Column(
            children: <Widget>[
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
              RoutineCard(),
            ],
          ),
        ),
      ),
    );
  }

}