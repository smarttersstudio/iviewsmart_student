import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget{
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    Container _getInfo(String title, String info, String time){
        return Container(
          width: MediaQuery.of(context).size.width,
          child: Card(
            elevation: 0.0,
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage('account_logo.png'),
                      radius: 30.0,
                    ),
                    RichText(
                      text: TextSpan(
                        text: title,
                        style: TextStyle(color: Colors.black,fontSize: 16.0, ),
                        children: [
                          TextSpan(text: '\n'),
                          TextSpan(
                            style: TextStyle(color: Colors.grey,fontSize: 11.0, ),
                            text: info,
                          )
                        ],
                      ),
                    ),
                    Container(
                   //   margin: EdgeInsets.only(left: MediaQuery.of(context).size.width/15),
                      child: Text(time,style: TextStyle(color: Colors.grey,fontSize: 12.0),),
                    )
                  ],
                ),
                SizedBox(
                  child: Container(
                    margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/190,top: MediaQuery.of(context).size.height/100),
                    color: Colors.grey,
                    height: 1.0,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                  ),
                ),
              ],
            ),
          ),
        );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Notifications', style: TextStyle(color: Colors.black),),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        iconTheme: IconThemeData(
          color: Colors.blueAccent,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              _getInfo("Your fees is pending!! ", "Last day of submission is 25th Feb", "11:39 AM"),
              _getInfo("Your fees is pending!! ", "Last day of submission is 25th Feb", "11:39 AM")
            ],
          ),
        ),
      ),
    );
  }
}