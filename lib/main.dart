import 'package:flutter/material.dart';
import 'package:iviewsmart_student/pages/main_page.dart';
import 'package:iviewsmart_student/pages/splash_page.dart';

void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: TextTheme(
          button: TextStyle(
            backgroundColor: Colors.blue,
            color: Colors.white,
            fontSize: 22.0,
            fontFamily: 'Roboto'
          ),
        ),
      ),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        'mainpage': (BuildContext context) => MainPage(),
      }
    );
  }
}