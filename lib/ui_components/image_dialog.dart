import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:dio/dio.dart';
import 'package:permission_handler/permission_handler.dart';

class ImageDialog extends StatefulWidget{
  final _data;
  final int _position;
  ImageDialog(this._data, this._position);

  @override
  _ImageDialogState createState() => _ImageDialogState();
}

class _ImageDialogState extends State<ImageDialog> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isDownloading = false;
  var progress;
  String imageUrl = 'https://images.pexels.com/photos/311716/pexels-photo-311716.jpeg?cs=srgb&dl=blur-card-city-311716.jpg&fm=jpg';

  _getPhotos() {
    List<PhotoViewGalleryPageOptions> list = List();
    for (int i = 0;  i < 7;  i++) {
      list.add(
        PhotoViewGalleryPageOptions(
          //imageProvider: AssetImage('background.jpg'),
          imageProvider: NetworkImage(imageUrl),
        ),
      );
    }
    return list;
  }

  @override
  void initState() {
    if (isDownloading) {
      showDialog(context: context,
          builder: (context) {
            return Dialog(
              backgroundColor: Colors.transparent,
              child: Container(
                height: 100.0,
                width: 200.0,
                alignment: Alignment.center,
                color: Colors.pinkAccent,
                child: Text('Downloading.... $progress',
                  style: TextStyle(color: Colors.white, fontSize: 20.0),),
              ),
            );
          }
      );
    }
    super.initState();
  }

  Future<bool> _checkPermission() async {
    if (Theme.of(context).platform == TargetPlatform.android) {
      PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);
      if (permission != PermissionStatus.granted) {
        Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([PermissionGroup.storage]);
        if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
          return true;
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  Future<void> downloadFile() async{
    Dio dio = Dio();
      if(await _checkPermission()){
        try{
          var directory = Theme.of(context).platform == TargetPlatform.android
              ? await getExternalStorageDirectory()
              : await getApplicationDocumentsDirectory();
          var saveDir = directory.path +'/Download/image1.jpg';

          await dio.download(imageUrl, saveDir,
              onReceiveProgress: (rec, total){
                setState(() {
                  isDownloading = true;
                  progress = ((rec/total)*100).toStringAsFixed(0)+' %';
                });
              });
        }catch(ex){
          print(ex.toString());
          _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(ex.toString())));
        }
        setState(() {
          isDownloading = false;
          progress = "Download complete!";
        });
      }else{
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Permission not granted!')));
      }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
       backgroundColor: Colors.black,
       actions: <Widget>[
         Padding(
           padding: const EdgeInsets.all(8.0),
           child: InkWell(
             onTap: () {
               downloadFile();
             },
             child: Row(
               children: <Widget>[
                 Icon(
                   Icons.save_alt,color: Colors.white,
                 ),
                 Text('Save',
                  style: TextStyle(color: Colors.white),
                 ),
               ],
             ),
           ),
         ),
       ],
      ),
      body: isDownloading ? Dialog(
        insetAnimationCurve: Curves.elasticInOut,
        backgroundColor: Colors.transparent,
        child: Container(
          height: 100.0,
          width: 200.0,
          alignment: Alignment.center,
          color: Colors.black54,
          child: Text('Downloading.... $progress',
            style: TextStyle(color: Colors.white, fontSize: 20.0),),
        ),
      ) :
      PhotoViewGallery(
        enableRotation: true,
        pageOptions: _getPhotos(),
        pageController: PageController(
          initialPage: widget._position,
        ),
        backgroundDecoration: BoxDecoration(color: Colors.black),
      ),
    );
  }
}