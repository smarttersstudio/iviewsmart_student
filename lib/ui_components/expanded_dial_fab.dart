import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class ExpandedFab extends StatefulWidget{
  @override
  _ExpandedFabState createState() => _ExpandedFabState();
}
class _ExpandedFabState extends State<ExpandedFab> with TickerProviderStateMixin{
  ScrollController _scrollController;
  bool _dialVisible = true;
  bool _opened = false;
  initState() {
    super.initState();

    _scrollController = ScrollController()
      ..addListener(() {
        _setDialVisible(_scrollController.position.userScrollDirection == ScrollDirection.forward);
      });
  }
  _setDialVisible(bool value) {
    setState(() {
      _dialVisible = value;
    });
  }

  _renderSpeedDial() {
    return SpeedDial(
      //animatedIcon: AnimatedIcons.menu_close,
      child: _opened ? Icon(Icons.close) : Icon(Icons.add),
      animatedIconTheme: IconThemeData(size: 22.0),
      onOpen: () {
        setState(() {
          _opened = true;
        });
        },
      onClose: () {
        setState(() {
          _opened = false;
        });
        },
      visible: _dialVisible,
      elevation: 4.0,
      //curve: Curves.easeIn,
      children: [
        SpeedDialChild(
          child: Icon(Icons.edit, color: Colors.white),
          backgroundColor:Color(0xffFF9A3E),
          onTap: () {

          },
          label: 'Add Notes',
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.white,
          elevation: 3.0,
        ),
        SpeedDialChild(
          child: Icon(Icons.videocam, color: Colors.white),
          backgroundColor: Color(0xffE50A4A),
          onTap: () {

          },
          label: 'Add Videos',
          elevation: 3.0,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.white,
        ),
        SpeedDialChild(
          child: Icon(Icons.photo, color: Colors.white),
          backgroundColor: Color(0xff6600FD),
          onTap: () => (){

          },
          label: 'Add Photos',
          elevation: 3.0,
          labelStyle: TextStyle(fontWeight: FontWeight.w500),
          labelBackgroundColor: Colors.white,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return  _renderSpeedDial();
  }
}