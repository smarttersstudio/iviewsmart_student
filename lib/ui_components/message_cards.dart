import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/custom_paint.dart';

class GroupSenderCard extends StatelessWidget {
  final String time;
  final String message;

  GroupSenderCard(this.time, this.message);

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      alignment: Alignment.centerRight,
      margin: EdgeInsets.only(left: _width/5, right: _width/10, top: 10.0),
      child: Wrap(
        direction: Axis.vertical,
        children: <Widget>[
          CustomPaint(
            painter: ShapesPainter(
              color: Colors.blue,
              isSender: true,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
//              decoration: BoxDecoration(
//                color: Colors.blue.withOpacity(0.8),
//                borderRadius: BorderRadius.circular(4.0),
//              ),
              child: Wrap(
                direction: Axis.vertical,
                children: <Widget>[
                  Text('You',
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.white70,
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(minWidth: 10.0, maxWidth: _width/2),
                    child: Text(message,
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 3.0),
            child: Text(time,
              style: TextStyle(
                fontSize: 8.0,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class GroupReceiverCard extends StatelessWidget {
  final String sender;
  final String time;
  final String message;

  GroupReceiverCard(this.sender, this.time, this.message);

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.only(left: _width/10, right: _width/10, top: 10.0),
      child: Wrap(
        direction: Axis.vertical,
        children: <Widget>[
          CustomPaint(
            painter: ShapesPainter(
              color: Colors.white,
              isSender: false,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: Wrap(
                direction: Axis.vertical,
                children: <Widget>[
                  Text(sender,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.red,
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(minWidth: 20.0, maxWidth: _width/2),
                    child: Text(message,
                      style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.black
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 3.0),
            child: Text(time,
              style: TextStyle(
                fontSize: 8.0,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PersonalSenderCard extends StatelessWidget {
  final String time;
  final String message;

  PersonalSenderCard( this.time, this.message);

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      alignment: Alignment.centerRight,
      margin: EdgeInsets.only(left: _width/10, right: _width/10, top: 10.0),
      child: Wrap(
        direction: Axis.vertical,
        children: <Widget>[
          CustomPaint(
            painter: ShapesPainter(
              color: Colors.blue,
              isSender: true,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: ConstrainedBox(
                constraints: BoxConstraints(minWidth: 10.0, maxWidth: _width/2),
                child: Text(message,
                  style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 3.0),
            child: Text(time,
              style: TextStyle(
                fontSize: 8.0,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PersonalReceiverCard extends StatelessWidget {
  final String time;
  final String message;

  PersonalReceiverCard(this.time, this.message);

  @override
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      margin: EdgeInsets.only(left: _width/10, right: _width/10, top: 10.0),
      child: Wrap(
        direction: Axis.vertical,
        children: <Widget>[
          CustomPaint(
            painter: ShapesPainter(
              color: Colors.white,
              isSender: false,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
              child: ConstrainedBox(
                constraints: BoxConstraints(minWidth: 10.0, maxWidth: _width/2),
                child: Text(message,
                  style: TextStyle(
                      fontSize: 14.0,
                      color: Colors.black
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 3.0),
            child: Text(time,
              style: TextStyle(
                fontSize: 8.0,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
