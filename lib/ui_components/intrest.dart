import 'package:flutter/material.dart';
class InterestCard extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    double height=MediaQuery.of(context).size.height/3;
    double width=MediaQuery.of(context).size.width;
    _getNameUi(String name, String section, String url) {
      return Container(
        height: height/6,
        width: width,
        margin: EdgeInsets.only(left:10.0,top: 5.0,bottom: 2.0),
        child: Row(
          children: <Widget>[
            CircleAvatar(
              backgroundImage:AssetImage('account_logo.png'),
            ),
            Container(
              margin: EdgeInsets.only(left:8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(name,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18.0
                    ),
                  ),
                  Text(
                    section,textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 15.0,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    }
    return Card(
      elevation: 3.0,
      child: Container(
        width: width,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _getNameUi("Name","class VIII C","url"),
            Container(
              margin: EdgeInsets.only(left: 15.0,bottom: 5.0),
              child: Text("caption",
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontSize: 15.0,
                  fontStyle: FontStyle.italic,
                  color: Colors.black
                ),
              ),
            ),
            Container(
                height: height*2/3,
                width: width,
                child: Image.asset(
                    'dashboard_background.png',
                  fit: BoxFit.fill,
                )
            ),
            Container(
              margin: EdgeInsets.all(5.0),
              child: Row(
                children: <Widget>[
                  Icon(Icons.favorite,color: Colors.grey,),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text('10k likes',
                    style: TextStyle(color: Colors.grey),),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0,right: 15.0,top:5.0,bottom: 10.0),
              width: width,
              height: height/6,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[300])
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(Icons.favorite,color: Colors.red,),
                      Container(child: Text('Like'),margin: EdgeInsets.only(left: 5.0),)
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.share,color: Colors.grey,),
                      Container(child: Text('Share'),margin: EdgeInsets.only(left: 5.0),)
                    ],
                  ),
                  Row(
                    children: <Widget>[
                  Image.asset('whatsapp_logo.png',
                    color: Colors.green,
                    height: height/12,
                    fit: BoxFit.fill,
                  ),
                  Container(child: Text('Share on Whatsapp'),margin: EdgeInsets.only(left: 5.0),)
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );}



}