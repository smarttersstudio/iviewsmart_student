import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

final List<String> imgList = [
  'https://images.pexels.com/photos/788662/pexels-photo-788662.jpeg?cs=srgb&dl=ads-architecture-buildings-788662.jpg&fm=jpg',
  'https://images.pexels.com/photos/341014/pexels-photo-341014.jpeg?cs=srgb&dl=ad-architecture-brick-wall-341014.jpg&fm=jpg',
  'https://images.pexels.com/photos/788662/pexels-photo-788662.jpeg?cs=srgb&dl=ads-architecture-buildings-788662.jpg&fm=jpg',
  'https://images.pexels.com/photos/341014/pexels-photo-341014.jpeg?cs=srgb&dl=ad-architecture-brick-wall-341014.jpg&fm=jpg',
  'https://images.pexels.com/photos/788662/pexels-photo-788662.jpeg?cs=srgb&dl=ads-architecture-buildings-788662.jpg&fm=jpg',
  'https://images.pexels.com/photos/341014/pexels-photo-341014.jpeg?cs=srgb&dl=ad-architecture-brick-wall-341014.jpg&fm=jpg',
];

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

final Widget placeholder = Container(color: Colors.grey);



class CarouselWithIndicator extends StatefulWidget {
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {

    final List child = map<Widget>(
      imgList, (index, i) {
      return Container(
        margin: EdgeInsets.all(5.0),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          child: CachedNetworkImage(
            placeholder: (context, url) => Icon(Icons.image),
            errorWidget: (context, url, error) => Icon(Icons.error),
            imageUrl: i,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
        ),
      );
    },
    ).toList();

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
        children: [
      CarouselSlider(
        height: MediaQuery.of(context).size.height/4.5,
        items: child,
        autoPlay: true,
        enlargeCenterPage: true,
        aspectRatio: 2.0,
        onPageChanged: (index) {
          setState(() {
            _current = index;
          });
        },
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: map<Widget>(
          imgList,
              (index, url) {
            return Container(
              width: 5.0,
              height: 5.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                      ? Colors.blue
                      : Colors.grey),
            );
          },
        ),
      ),
    ]);
  }
}