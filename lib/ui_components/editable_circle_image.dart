import 'package:flutter/material.dart';

class EditableCircleImage extends StatefulWidget{

  final Function onPressed;
  final ImageProvider backgroundImage;
  final double size;
  final IconData icon;

  EditableCircleImage({
      this.onPressed,
      this.backgroundImage,
      this.size = 50.0,
      this.icon,
      Key key,}):super(key : key);

  @override
  _EditableCircleImageState createState() => _EditableCircleImageState();

}

class _EditableCircleImageState extends State<EditableCircleImage> {
  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: widget.size,
          height: widget.size,
          child: CircleAvatar(
            backgroundImage: widget.backgroundImage != null ? widget.backgroundImage : AssetImage('logo.png'),
          ),
        ),
        Positioned(
          bottom: 2.0,
          right: 2.0,
          child: InkWell(
            onTap: widget.onPressed,
            child: Container(
              width: widget.size/3,
              height: widget.size/3,
              child: CircleAvatar(
                backgroundColor: Colors.blueAccent,
                child: Icon(widget.icon != null ? widget.icon : Icons.edit , size: widget.size/5,color: Colors.white,),
              ),
            ),
          ),
        ),
      ],
    );
  }
}