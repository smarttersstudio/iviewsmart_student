import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/editable_circle_image.dart';

class ProfilePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    Container _getInfo(String title,String info){
      return Container(
        alignment: Alignment.centerLeft,
        height: _height/12,
        width: _width,
        child: RichText(
          text: TextSpan(
            text: title,
            style: TextStyle(color: Colors.grey,fontSize: 14.0, ),
            children: [
              TextSpan(text: '\n'),
              TextSpan(
                style: TextStyle(color: Colors.black,fontSize: 16.0,fontWeight: FontWeight.bold ),
                text: info,
              ),
            ],
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Text('Profile',
              style: TextStyle(color: Colors.black)
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: _width/11, right: _width/11),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height/20, bottom: MediaQuery.of(context).size.height/40),
                child: Row(
                  children: <Widget>[
                    EditableCircleImage(
                      backgroundImage: AssetImage('account_logo.png'),
                      size: _width/5,
                      icon: Icons.edit,
                      onPressed: (){

                      },
                    ),
                    SizedBox(
                      width: 20.0,
                    ),
                    RichText(
                      text: TextSpan(
                        text: 'Rishabh Shukla',
                        style: TextStyle(color: Colors.black,fontSize: 20.0, ),
                        children: [
                          TextSpan(text: '\n'),
                          TextSpan(
                            style: TextStyle(color: Colors.grey,fontSize: 12.0, ),
                            text: 'Enrolment ID : 123456789',
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                child: Container(
                  margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height/40),
                  color: Colors.grey,
                  height: 1.0,
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
              _getInfo("Date Of Birth", "25-Sept-1997"),
//              SizedBox(
//                height: 3.0,
//              ),
              _getInfo("Father Name", "Dinesh Chand Shukla"),
//              SizedBox(
//                height: 3.0,
//              ),
              _getInfo("Mother Name", "Prativa Shukla"),
//              SizedBox(
//                height: 3.0,
//              ),
              _getInfo("Contact Number", "+91-696969"),
//              SizedBox(
//                height: 5.0,
//                ),
              _getInfo("School Name", "IMCA School"),
//              SizedBox(
//                height: 3.0,
//              ),
              _getInfo("Class And Section", "XII-C")
            ],
          ),
        ),
      ),
    );
  }

}