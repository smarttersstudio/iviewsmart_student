import 'package:flutter/material.dart';
import 'package:iviewsmart_student/home_app_bar_pages/news_feed/tabs/international_news_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/news_feed/tabs/local_news_page.dart';
import 'package:iviewsmart_student/home_app_bar_pages/news_feed/tabs/national_news_page.dart';

class NewsFeedPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(child: Text('Local News',
                style: TextStyle(color: Colors.black, fontSize: width/25,),
              ),
              ),
              Tab(child: Text('National',
                style: TextStyle(color: Colors.black, fontSize: width/25,),
              ),
              ),
              Tab(child: Text('International',
                style: TextStyle(color: Colors.black, fontSize: width/25,),
              ),
              ),
            ],
          ),
          title: Text('News Feed',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: TabBarView(
            //physics: NeverScrollableScrollPhysics(),
            children: [
              LocalNewsPage(),
              NationalNewsPage(),
              InternationalNewsPage()
            ]
        ),
      ),
    );
  }
}