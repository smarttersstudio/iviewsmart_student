import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/intrest.dart';

class LocalNewsPage extends StatefulWidget{
  @override
  _LocalNewsPageState createState() => _LocalNewsPageState();
}

class _LocalNewsPageState extends State<LocalNewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              InterestCard(),
              InterestCard(),
              InterestCard(),
              InterestCard(),
            ],
          ),
        ),
      ),
    );
  }
}