import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/intrest.dart';

class InternationalNewsPage extends StatefulWidget{
  @override
  _InternationalNewsPageState createState() => _InternationalNewsPageState();
}

class _InternationalNewsPageState extends State<InternationalNewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              InterestCard(),
              InterestCard(),
              InterestCard(),
              InterestCard(),
            ],
          ),
        ),
      ),
    );
  }
}