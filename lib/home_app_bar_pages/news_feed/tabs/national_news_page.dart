import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/intrest.dart';

class NationalNewsPage extends StatefulWidget{
  @override
  _NationalNewsPageState createState() => _NationalNewsPageState();
}

class _NationalNewsPageState extends State<NationalNewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:SingleChildScrollView(
          child: Column(
            children: <Widget>[
              InterestCard(),
              InterestCard(),
              InterestCard(),
              InterestCard(),
            ],
          ),
        ),
      ),
    );
  }
}