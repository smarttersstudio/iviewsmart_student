import 'package:flutter/material.dart';
import 'package:iviewsmart_student/ui_components/expanded_dial_fab.dart';
import 'package:iviewsmart_student/ui_components/intrest.dart';

class InterestPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('Section Of Interest',
            style: TextStyle(color: Colors.black)
        ),
        backgroundColor: Colors.white,
        elevation: 4.0,),
      floatingActionButton: ExpandedFab(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            InterestCard(),
            InterestCard(),
            InterestCard(),
            InterestCard()
          ],
        ),
      )
    );
  }
}